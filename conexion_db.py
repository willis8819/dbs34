import psycopg2

class conexion_db:
    def __init__(self,mi_host,database,user,passwd): 
        try:
            self.conn = psycopg2.connect(   
                host="localhost",#al tratarse de la db alojada en server local
                database="Transportes", #nombre de la db
                user="Chiquito",#nombre de ususario (por defecto es postgres)
                password="blacktonder")#contraseña configuracion en instalacion
            self.cur = self.conn.cursor()
        except:
            print("Error en la conexion")

    def consultar_db(self,query):
        try:
            self.cur.execute(query)
            response= self.cur.fetchall()
            return response
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)

    def escribir_db(self,query):
        try:
            self.cur.execute(query)
            self.conn.commit()
        except(Exception, psycopg2.DatabaseError) as error:
            print(error)
    
    def cerrar_db(self):
        self.cur.close()
        self.conn.close()
